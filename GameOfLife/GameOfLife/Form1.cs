﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameOfLife
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// The checkboxes displayed on screen.
        /// </summary>
        private CheckBox[,] _world;

        /// <summary>
        /// A thread that steps the world until killed.
        /// </summary>
        private Thread _gameThread;

        /// <summary>
        /// The file name that was loaded or is being edited.
        /// </summary>
        private string _filename;

        public Form1()
        {
            InitializeComponent();

            //Build the "world".
            var width = (this.Width - 20) / 15;
            var height = (this.Height - 75) / 15;
            width -= 1;
            height -= 1;
            _world = new CheckBox[width,height];
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    var cb = new CheckBox {Location = new Point(5 + (15 * x), 25 + (15 * y)), Width = 15, Height = 15};
                    this.Controls.Add(cb);
                    _world[x, y] = cb;
                }
            }
        }

        /// <summary>
        /// Kill the game thread if disposing the window to prevent use after dispose errors.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            _gameThread?.Interrupt();
        }

        /// <summary>
        /// Sets the title and the "save" path.
        /// </summary>
        /// <param name="filename"></param>
        private void SetFileName(string filename)
        {
            _filename = filename;
            this.Text = filename == null ? "The Game of Life" : $"The Game of Life - {Path.GetFileName(_filename)}";
        }

        /// <summary>
        /// Converts the checkboxes into an 2D array of bools so the checkboxes can be updated without mutating the evolution state data.
        /// </summary>
        /// <returns></returns>
        private bool[,] GetStates()
        {
            var ret = new bool[_world.GetLength(0), _world.GetLength(1)];
            for(var x = 0; x < ret.GetLength(0); x++)
            for (var y = 0; y < ret.GetLength(1); y++)
                ret[x, y] = _world[x, y].Checked;

            return ret;
        }

        /// <summary>
        /// Counts the neighbours that are living next to the specified cell.
        /// </summary>
        /// <param name="x">X pos</param>
        /// <param name="y">Y pos</param>
        /// <param name="states">Evolution state data</param>
        /// <returns>A count of living cells bordering this one.</returns>
        public int CountLivingNeighbours(int x, int y, bool[,] states)
        {
            var count = 0;
            if (x > 0)
            {
                if (states[x - 1, y])
                    count++;
                if (y > 0 && states[x - 1, y - 1])
                    count++;
                if (y < states.GetLength(1) - 1 && states[x - 1, y + 1])
                    count++;
            }

            if (x < states.GetLength(0) - 1)
            {
                if (states[x + 1, y])
                    count++;
                if (y > 0 && states[x + 1, y - 1])
                    count++;
                if (y < states.GetLength(1) - 1 && states[x + 1, y + 1])
                    count++;
            }

            if (y > 0 && states[x, y - 1])
                count++;
            if (y < states.GetLength(1) - 1 && states[x, y + 1])
                count++;
            return count;
        }

        /// <summary>
        /// Sets a checkbox state by performing an invoke to transfer control to the UI thread.
        /// </summary>
        /// <param name="x">X pos</param>
        /// <param name="y">Y pos</param>
        /// <param name="state">New state.</param>
        public void SetCheckState(int x, int y, bool state)
        {
            Invoke((MethodInvoker) delegate { _world[x, y].Checked = state; });
        }

        /// <summary>
        /// Steps the world by one.
        /// </summary>
        public void StepWorld()
        {
            var states = GetStates();
            for (var x = 0; x < _world.GetLength(0); x++)
            {
                for (var y = 0; y < _world.GetLength(1); y++)
                {
                    var neighbours = CountLivingNeighbours(x, y, states);
                    if (neighbours < 2)
                        SetCheckState(x, y, false);
                    else if (neighbours > 3)
                        SetCheckState(x, y, false);
                    else if (neighbours == 3 && !_world[x, y].Checked)
                        SetCheckState(x, y, true);
                }
            }
        }

        /// <summary>
        /// Thread start for the game thread. Uses <see cref="ThreadInterruptedException"/> to exit the loop.
        /// </summary>
        private void ThreadLoop()
        {
            try
            {
                while (true)
                {
                    StepWorld();
                }
            }
            catch (ThreadInterruptedException ex)
            {
                //Swallow exception because this is normal.
            }
            catch (Exception ex)
            {
                MessageBox.Show($"General Failure in evolution loop. Ending loop.\n\n{ex.Message}", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //Update the UI.
                try
                {
                    Invoke((MethodInvoker) delegate { playToolStripMenuItem.Checked = false; });
                }
                catch (Exception ex)
                {
                    //Swallow because I don't care.
                }
            }
        }

        /// <summary>
        /// Fired when the UI button to step is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stepToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StepWorld();
        }

        /// <summary>
        /// Fired when the UI button to reset the world is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_filename == null)
            {
                for (var x = 0; x < _world.GetLength(0); x++)
                {
                    for (var y = 0; y < _world.GetLength(1); y++)
                    {
                        _world[x, y].Checked = false;
                    }
                }
            }
            else
            {
                LoadFromFile(_filename, false);
            }
        }

        /// <summary>
        /// Fired when the UI button to play the world is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void playToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!((ToolStripMenuItem)sender).Checked)
            {
                _gameThread = new Thread(ThreadLoop);
                _gameThread.Name = "Game Thread";
                _gameThread.Start();
                ((ToolStripMenuItem) sender).Checked = true;
            }
            else
            {
                _gameThread.Interrupt();
                _gameThread = null;
                ((ToolStripMenuItem) sender).Checked = false;
            }
        }

        /// <summary>
        /// Fired when the UI button to exit is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Saves the current world state to a file.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="notify"></param>
        private void SaveToFile(string path, bool notify = true)
        {
            if (File.Exists(path))
                File.Delete(path);
            var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
            var sw = new BinaryWriter(fs);
            sw.Write(_world.GetLength(0));
            sw.Write(_world.GetLength(1));
            for (var x = 0; x < _world.GetLength(0); x++)
            {
                for (var y = 0; y < _world.GetLength(1); y++)
                {
                    sw.Write(_world[x, y].Checked);
                }
            }
            SetFileName(path);
            if(notify)
                MessageBox.Show("File saved.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Load the world state from a file.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="notify"></param>
        private void LoadFromFile(string path, bool notify = true)
        {
            var fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            var sw = new BinaryReader(fs);
            sw.ReadInt32();
            sw.ReadInt32();
            for (var x = 0; x < _world.GetLength(0); x++)
            {
                for (var y = 0; y < _world.GetLength(1); y++)
                {
                    _world[x, y].Checked = sw.ReadBoolean();
                }
            }
            SetFileName(path);
            if(notify)
                MessageBox.Show("File loaded.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Fired when the UI button to save as is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (playToolStripMenuItem.Checked)
            {
                MessageBox.Show("Cannot save while running world. Please stop running world to save.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SaveToFile(saveFileDialog1.FileName);
            }
        }

        /// <summary>
        /// Fired when the UI button to load is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (playToolStripMenuItem.Checked)
            {
                MessageBox.Show("Cannot load while running world. Please stop running world to load.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LoadFromFile(openFileDialog1.FileName);
            }
        }

        /// <summary>
        /// Fired when the UI button to save is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (playToolStripMenuItem.Checked)
            {
                MessageBox.Show("Cannot save while running world. Please stop running world to save.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (_filename == null)
                saveAsToolStripMenuItem_Click(sender, e);
            else
            {
                SaveToFile(_filename);
            }
        }

        /// <summary>
        /// Fired when the UI button to create a new world is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetFileName(null);
            resetToolStripMenuItem_Click(sender, e);
        }

        /// <summary>
        /// Fired when the UI button to show the about info is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("(C) Ned Hyett 2020\n\nAll rights reserved.", "About", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }
    }
}
